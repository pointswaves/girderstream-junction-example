# A basic Girderstream project

This project uses freedesktop-sdk as its too chain and produces minimal systems

## Running a local cas

Girderstream needs you to run a local cas for it to build with

```
buildbox-casd  --bind localhost:50040 ~/.cache/girderstream/cas
```

## Bootstrap-ing

You can use the remote cas to provide the boot strap elements or you can
use a local checkout of FD_SDK to provide them.

To bootstrap this project you will need the freedesktop-sdk components
gzip, tar and wget.

```
casupload --cas-server=http://localhost:50040  components/wget2/ components/tar/ components/gzip/
```

But you may also need to bootstrap any projects that this project junctions.

## Using the project

You can check the state of a build with

```
girderstream --options bsp=x86 build show --name demo_app
```

Then you can build the elements over the junction

```
girderstream --options bsp=x86 build --name filesystem
```

Once you have build the project as above you can then checkout the parts needed to run the target system in qemu

```
girderstream --options bsp=x86 checkout --name kernel --location linux_x86
../girderstream/target/debug/girderstream --options bsp=x86 checkout --name filesystem --location filesystem_x86
```

Once the disk image and kernel have been checked out you can then use qemu to run the system

```
qemu-system-x86_64 -nographic -drive file=filesystem_x86/disk.img,format=raw -kernel linux_x86/boot/bzImage -append "earlyprintk=serial,ttyS0 console=ttyS0 root=/dev/sda2 rw"
```

Once the emulated system has fully booted up you should get a prompt


```
/ #
```

To contrast with bst where if you build a top/packaging level element through a junction you can not
alter the element this project defines just `demo_rust.grd` and then injects it into the dependency tree of
the elements loaded by the junction.

In this case the `/usr/local/bin/hello` app from the junction is not present but `/usr/bin/girderstream_build_plugin_bst`
as defined in this project is present:

```
/ # girderstream_build_plugin_bst 
error: The following required arguments were not provided:
    --plugin-type <PLUGIN_TYPE>
    --variables-file <VARIABLES_FILE>

USAGE:
    girderstream_build_plugin_bst --plugin-type <PLUGIN_TYPE> --variables-file <VARIABLES_FILE>

For more information try --help
/ # 
```

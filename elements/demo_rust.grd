kind: bootstrap_manual

provides: demo_app

depends:
- dependency: tool_chain
  scope: Build
- dependency: tool_chain_cross
  scope: Build
- dependency: tool_chain_cross_libs
  scope: Build
- dependency: tool_chain_rust_arm
  scope: Build
- dependency: tool_chain_llvm
  scope: Build

sources:
- kind: cargo_source
  url: https://gitlab.com/pointswaves/girderstream-build-plugin
  sha: d51c49aa3a4d087f5fbc02bbc1aa591434902740

context: build_target

plugin_variables:
  join_strategy: Join
  variables:
    config_install-commands: |
      HOME="/sources" cargo install --path . --offline --root /buildstream-install/usr/ %{cargo_args_extra}
      rm /buildstream-install/usr/.crates2.json
      rm /buildstream-install/usr/.crates.toml

    # this bit should go in the plugin definition rather than here
    # but as this is a bit work in progress its ok here
    cli: |
      mkdir -p /sources/
      mkdir -p /output_dir/
      cd /sources/
      girderstream_build_plugin_bst -v /plugin_vars.yaml -p /manual.yaml
      # the /plugin_vars.yaml comes from girderstream and happens for all plugins
      # the /girderstream_build_plugin_bst and /manual.yaml come from the plugin.
      # The plugin also provides cmake.yaml and autotools.yaml and they contain
      # the templates for thier equivalent bst build plugins.
      # You can use any bst build plugin that is just yaml in this way, eg:
      # /girderstream_build_plugin_bst -v /plugin_vars.yaml -p /any_bst_plugin_yaml.yaml
      #
      # The file /plugin_vars.yaml comes from girderstream and contains all the
      # other variables from the project etc, /girderstream_build_plugin_bst then
      # does the variable substitution that buildstream would do in the plugin
      # base class.
      #
      # Once they are combined it creates ./configured.sh which we can run
      # to get the same effect as any bst build plugin that just used .yaml build.
      # Plugins that have more than the basic .py file will need more converting
      # to Girderstream.
      chmod u+x configured.sh

      cat >.cargo/config <<EOF
      [target.aarch64-unknown-linux-gnu]
      linker = "aarch64-unknown-linux-gnu-gcc"
      EOF
      ls /usr/lib/sdk/toolchain-aarch64/bin/aarch64-unknown-linux-gnu-gcc
      ls
      cat configured.sh
      ./configured.sh
      cp -R /buildstream-install/* /output_dir/
